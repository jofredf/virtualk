#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	cout << "MAIN: VirtualK (" << getpid() << ") version " << VER_MAJOR << "." 
			<< VER_MINOR << "." << VER_REVISION << endl;
	
	#ifdef DEBUG
	cout << "MAIN: DEBUG mode is ON" << endl;
	#endif

	return EXIT_SUCCESS;
}

